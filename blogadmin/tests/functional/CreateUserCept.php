<?php
$I = new FunctionalTester($scenario);
$I->am('admin');
$I->wantTo('create a new user');

//When
$I->amOnPage('/admin/users');
$I->see('Users', 'h1');
$I->dontSee('Jake Paterson');
//And
$I->click('add User');


//Then
$I->amOnPage('/admin/users/create');
//And
$I->see('Add User', 'h1');
$I->submitForm('.createuser',[
  'name' => 'Jake Paterson',
  'email'=> 'jakepaterson97@gmail.com',
  'password'=> 'password'
]);
//Then
$I->seeCurrentUrlEqals('/admin/users');
$I->see('Users', 'h1');
$I->see('New user added');
$I->see('Jake Paterson');

//check for duplicates

//When

$I->amOnPage('/admin/users');
$I->see('Users', 'h1');
$I->dontSee('Jake Paterson');
//And
$I->click('add User');

//Then
$I->amOnPage('/admin/users/create');
//And
$I->see('Add User', 'h1');
$I->submitForm('.createuser',[
  'name' => 'Jake Paterson',
  'email'=> 'jakepaterson97@gmail.com',
  'password'=> 'password'
]);
//Then
$I->seeCurrentUrlEqals('/admin/users');
$I->see('Users', 'h1');
$I->see('Error user already exists!');
