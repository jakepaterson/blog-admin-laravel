<?php
$I = new FunctionalTester($scenario);
$I->am('a admin');
$I->wantTo('test Laravel is Working');

//When
$I->amOnPage('/');

//Then
$I->seeCurrentUrlEquals('/');
$I->see('Laravel 5', '.title');
