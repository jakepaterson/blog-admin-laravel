<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Articles</title>
  </head>
  <body>
    <h1>Articles</h1>
    <section>
      @if(isset($articles))
        <ul>
          @foreach($articles as $article)
            <li>{{$article->title}}</li>
          @endforeach
        </ul>
      @else
        <p>No articles added yet</p>
      @endif
    </section>

    {{!! Form::open([''])!!}}
      <div class="row">
        {!! Form::submit('Add Article', ['class'=>'button'])!!}
      </div>
    {{!! Form::close()!!}}
  </body>
</html>
